trigger ReservationOverlappingTrigger on Reservation__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    new ReservationsOverlappingTriggerHandler().run();
}