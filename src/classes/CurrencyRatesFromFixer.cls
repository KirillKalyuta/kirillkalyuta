/**
 * Created by kalyutak on 9/12/2018.
 */

public class CurrencyRatesFromFixer {

    public class Rates {
        public Double USD;
        public Double BYN;
        public Double JPY;
        public Double GBP;
    }

    public Boolean success;
    public Integer timestamp;
    public String base;
    public String date1;
    public Rates rates;


    public static CurrencyRatesFromFixer parse(String json) {
        return (CurrencyRatesFromFixer) System.JSON.deserialize(json, CurrencyRatesFromFixer.class);
    }
}
