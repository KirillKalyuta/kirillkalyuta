/**
 * Created by kalyutak on 8/24/2018.
 */
public class DefineLeadingOpp {

    public static void defineLeadingOpp(Set<Id> parSetIds) {
        if (!parSetIds.isEmpty()) {
            List<Opportunity> allOppList = new List<Opportunity>();
            List<Opportunity> parOppListWithLeadOpp = [
                SELECT Id, LeadingOpp__c, (
                    SELECT Id, IsLeading__c
                    FROM ChildOpp__r
                    ORDER BY IsWon DESC, Probability DESC, CreatedDate
                    LIMIT 1
                )
                FROM Opportunity
                WHERE Id IN :parSetIds
            ];
            allOppList.addAll(parOppListWithLeadOpp);
            List<Opportunity> childOpp = new List<Opportunity>();
            List<Id> idsListOfLeadChild = new List<Id>();
            for (Opportunity opp : parOppListWithLeadOpp) {
                if (!opp.ChildOpp__r.isEmpty()) {
                    opp.ChildOpp__r[0].IsLeading__c = true;
                    childOpp.add(opp.ChildOpp__r);
                    idsListOfLeadChild.add(opp.ChildOpp__r[0].Id);
                    opp.LeadingOpp__c = opp.ChildOpp__r[0].Id;
                }
            }
            allOppList.addAll(childOpp);
            List<Opportunity> childListOppWithLeading = [
                SELECT Id, IsLeading__c
                FROM Opportunity
                WHERE ParentOpp__r.Id IN :parSetIds
                AND IsLeading__c = TRUE
                AND Id NOT IN :idsListOfLeadChild
            ];
            for (Opportunity opp : childListOppWithLeading) {
                opp.IsLeading__c = false;
            }
            allOppList.addAll(childListOppWithLeading);
            update allOppList;
        }
    }
}