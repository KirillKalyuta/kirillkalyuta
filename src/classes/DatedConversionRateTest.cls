/**
 * Created by kalyutak on 9/20/2018.
 */

@IsTest
public class DatedConversionRateTest {

    private static final String endPointForFixer = 'callout:Creds_For_FixerIO';
    private static final String endPointForSF = 'callout:SalesForce/services/data/v43.0/composite/tree/DatedConversionRate/';

    private class DatedConversionRateMock implements HttpCalloutMock {

        public HttpResponse respond(HTTPRequest request) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            if (request.getEndpoint() == endPointForFixer) {
                response.setBody('{"success":true,"timestamp":1537446845,"base":"EUR","date":"2018-09-21","rates":{"USD":1.17715,"BYN":2.452886}}');
                response.setStatusCode(200);
            } else if (request.getEndpoint() == endPointForSF) {
                    response.setBody('{"records" :[{"startDate":"2018-09-21","isoCode":"USD","conversionRate":"1.174819","attributes":{"referenceId":"USD","type":"DatedConversionRate"}}]}');
                    response.setStatusCode(201);
                } else {
                    System.assertNotEquals(null, null, 'This End Point' + request.getEndpoint() + 'Not Supported.');
                }
            return response;
        }
    }

    @IsTest static void testUpdateRates() {
        Test.setMock(HttpCalloutMock.class, new DatedConversionRateMock());
        Test.startTest();
        HttpResponse response = DatedConversionRateScheduled.getRates();
        HttpResponse res = DatedConversionRateScheduled.insertRates();
        Test.stopTest();

        System.assertEquals(response.getStatusCode(), 200);
        System.assertEquals(res.getStatusCode(), 201);
    }

    @IsTest static void testDatedConversionRateScheduled() {
        Test.setMock(HttpCalloutMock.class, new DatedConversionRateMock());
        Test.startTest();
        new DatedConversionRateScheduled().execute(null);
        Test.stopTest();
    }
}