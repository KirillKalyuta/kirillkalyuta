/**
 * Created by kalyutak on 8/24/2018.
 */
public class OpportunityTriggerHandler extends TriggerHandler {

    public static final Id CHILD_RT;
    public static final Id PARENT_RT;

    private Map<Id, Opportunity> newMap;
    private Map<Id, Opportunity> oldMap;
    private List<Opportunity> newList;
    private List<Opportunity> oldList;

    static {
        CHILD_RT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
            .get('CompetingOppRT').getRecordTypeId();
        PARENT_RT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
            .get('ParentRT').getRecordTypeId();
    }

    public OpportunityTriggerHandler() {
        this.newMap = (Map<Id, Opportunity>) Trigger.newMap;
        this.oldMap = (Map<Id, Opportunity>) Trigger.oldMap;
        this.newList = (List<Opportunity>) Trigger.new;
        this.oldList = (List<Opportunity>) Trigger.old;
    }

    public override void afterInsert() {
        defineLeadingChildForPar(newList);
    }

    public override void afterUpdate() {
        defineLeadingChildForPar(newList);
        updateOliOnParentOpp();
    }

    public override void beforeDelete(){
        Set<Id> childIds = oldMap.keySet();
        List<OpportunityLineItem> childOli = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId IN : childIds];
        if (!childOli.isEmpty()) {
            Set<Id> oliIds = new Set<Id>();
            for (OpportunityLineItem oli : childOli) {
                oliIds.add(oli.Id);
            }
            delete [SELECT Id FROM OpportunityLineItem WHERE ExternalKey__c IN : oliIds];
        }
    }

    public override void afterDelete() {
        defineLeadingChildForPar(oldList);
    }

    public override void afterUndelete() {
        defineLeadingChildForPar(newList);
    }

    private void defineLeadingChildForPar(List<Opportunity> oppList) {
        Set<Id> parOppIds = new Set<Id>();
        if (Trigger.isAfter) {
            if (Trigger.isInsert || Trigger.isDelete || Trigger.isUndelete) {
                for (Opportunity opp : oppList) {
                    if (opp.RecordTypeId == CHILD_RT && opp.ParentOpp__c != null) {
                        parOppIds.add(opp.ParentOpp__c);
                    }
                }
                DefineLeadingOpp.defineLeadingOpp(parOppIds);
            }
            if (Trigger.isUpdate) {
                for (Opportunity oppNew : oppList) {
                    if (oppNew.RecordTypeId == CHILD_RT) {
                        Opportunity oppOld = oldMap.get(oppNew.Id);
                        if (oppNew.IsWon != oppOld.IsWon || oppNew.Probability != oppOld.Probability || oppNew.ParentOpp__c != oppOld.ParentOpp__c) {
                            if (oppNew.ParentOpp__c != null) {
                                parOppIds.add(oppNew.ParentOpp__c);
                            }
                            if (oppOld.ParentOpp__c != null) {
                                parOppIds.add(oppOld.ParentOpp__c);
                            }
                        }
                    }
                    if (oppNew.RecordTypeId != oldMap.get(oppNew.Id).RecordTypeId) {
                        parOppIds.add(oldMap.get(oppNew.Id).ParentOpp__c);
                    }
                }
                DefineLeadingOpp.defineLeadingOpp(parOppIds);
            }
        }
    }

    private void updateOliOnParentOpp() {
        Set<Id> parIds = new Set<Id>();
        for (Opportunity parOpp : newList) {
            if (parOpp.RecordTypeId == PARENT_RT && parOpp.LeadingOpp__c != oldMap.get(parOpp.Id).LeadingOpp__c) {
                parIds.add(parOpp.Id);
            }
        }
        if (!parIds.isEmpty()) {
            List<OpportunityLineItem> oliParList = [SELECT Id, ExternalKey__c FROM OpportunityLineItem WHERE OpportunityId IN :parIds];
            List<OpportunityLineItem> oliChilList = [SELECT Id FROM OpportunityLineItem WHERE Opportunity.ParentOpp__c IN :parIds];
            List<OpportunityLineItem> childOliOnPar = new List<OpportunityLineItem>();
            if (!oliParList.isEmpty()) {
                for (OpportunityLineItem oliPar : oliParList) {
                    for (OpportunityLineItem oliChild : oliChilList) {
                        if (oliPar.ExternalKey__c == oliChild.Id) {
                            childOliOnPar.add(oliPar);
                        }
                    }
                }
                delete childOliOnPar;
            }
            List<OpportunityLineItem> oliChildList = [
                SELECT Id, Name, Opportunity.ParentOpp__c, Quantity, TotalPrice, PricebookEntryId, OpportunityId, ExternalKey__c
                FROM OpportunityLineItem
                WHERE Opportunity.ParentOpp__c IN :parIds
                AND Opportunity.IsLeading__c = TRUE
            ];
            if (!oliChildList.isEmpty()) {
                List<OpportunityLineItem> oliParNewList = new List<OpportunityLineItem>();
                for (OpportunityLineItem oliChild : oliChildList) {
                    OpportunityLineItem oliPar = oliChild.clone(false, false, false, false);
                    oliPar.OpportunityId = oliChild.Opportunity.ParentOpp__c;
                    oliPar.ExternalKey__c = oliChild.Id;
                    oliParNewList.add(oliPar);
                }
                insert oliParNewList;
            }
        }
    }
}