/**
 * Created by kalyutak on 9/24/2018.
 */

public class DatedConversionRateWrapper {

    public List<Records> records { get; set; }

    public class Records {
        public Map<String, String> attributes { get; set; }
        public String isoCode { get; set; }
        public String conversionRate { get; set; }
        public String startDate { get; set; }
    }
}