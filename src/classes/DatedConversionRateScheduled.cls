/**
 * Created by kalyutak on 9/12/2018.
 */

global class DatedConversionRateScheduled implements Schedulable {

    private static final String endPointForFixer = 'callout:Creds_For_FixerIO';
    private static final String endPointForSF = 'callout:SalesForce/services/data/v43.0/composite/tree/DatedConversionRate/';
    private static String stringBodyForSFFull;

    global void execute(SchedulableContext param1) {
        updateCurrencyRates();
    }

    @future(Callout=true)
    private static void updateCurrencyRates() {
        getRates();
        insertRates();
    }

    public static HttpResponse getRates() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPointForFixer);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {
            Map<String, Object> rates = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            rates = (Map<String, Object>) rates.get('rates');
            createString(rates);
        } else {
            throw new InvalidStatusException('' + response);
        }
        return response;
    }

    public static HttpResponse insertRates() {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endPointForSF);
        req.setBody(stringBodyForSFFull);
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('POST');
        HttpResponse res = h.send(req);
        if (res.getStatusCode() == 401) {
            throw new InvalidStatusException('Check your assess in My Setting ==> Personal ==> Authentication Settings for External Systems');
        }
        return res;
    }

    private static void createString(Map<String, Object> rates) {
        DatedConversionRateWrapper dcrWrapper = new DatedConversionRateWrapper();
        List<DatedConversionRateWrapper.Records> records = new List<DatedConversionRateWrapper.Records>();
        for (String currentRate : rates.keySet()) {
            DatedConversionRateWrapper.Records item = new DatedConversionRateWrapper.Records();
            item.isoCode = currentRate;
            item.startDate = String.valueOf(Date.today());
            item.conversionRate = String.valueOf(rates.get(currentRate));
            item.attributes = new Map<String, String>{'referenceId' => currentRate, 'type' => 'DatedConversionRate'};
            records.add(item);
        }
        dcrWrapper.records = records;
        stringBodyForSFFull = JSON.serialize(dcrWrapper);
    }

    public class InvalidStatusException extends Exception{}
}