/**
 * Created by kalyutak on 9/3/2018.
 */
@isTest
public class ReservationsOverlappingTriggerTest {

    @testSetup static void setup() {
        List<House__c> houseList = new List<House__c>();
        for (Integer i = 0; i < 2; i++) {
            houseList.add(new House__c(Name = 'Test' + i));
        }
        insert houseList;

        Date startDate = Date.today();
        List<Reservation__c> reservations = new List<Reservation__c>();
        for (Integer k = 0; k < houseList.size(); k++) {
            House__c house = houseList[k];
            reservations.add(new Reservation__c(HouseName__c = house.Id, StartDate__c = startDate, EndDate__c = startDate.addDays(4)));
            reservations.add(new Reservation__c(HouseName__c = house.Id, StartDate__c = startDate.addDays(8), EndDate__c = startDate.addDays(15)));
        }
        insert reservations;
    }

    @isTest static void beforeInsertTest() {
        Date startDate = Date.today();
        House__c house = [SELECT Id FROM House__c LIMIT 1];
        Reservation__c newReservation = new Reservation__c(HouseName__c = house.Id, StartDate__c = startDate, EndDate__c = startDate.addDays(3));
        System.assertNotEquals(Database.insert(newReservation, false).isSuccess(), true);
        System.assertNotEquals(Database.insert(newReservation, false).getErrors()[0].getMessage(), null);

        Reservation__c newReservation2 = new Reservation__c(HouseName__c = house.Id, StartDate__c = startDate.addDays(20), EndDate__c = startDate.addDays(23));
        System.assertEquals(Database.insert(newReservation2, false).isSuccess(), true);


    }

    @isTest static void beforeUpdateTest() {
        Date startDate = Date.today();
        Reservation__c reservation = [SELECT Id, EndDate__c, StartDate__c,HouseName__c FROM Reservation__c WHERE StartDate__c = :startDate LIMIT 1];
        reservation.StartDate__c = startDate.addDays(8);
        reservation.EndDate__c = startDate.addDays(13);
        System.assertNotEquals(Database.update(reservation, false).isSuccess(), true);
        System.assertNotEquals(Database.update(reservation, false).getErrors()[0].getMessage(), null);

        reservation.StartDate__c = startDate.addDays(20);
        reservation.EndDate__c = startDate.addDays(21);
        System.assertEquals(Database.update(reservation, false).isSuccess(), true);
    }

    @isTest static void afterUndeleteTest() {
        Date startDate = Date.today();
        Reservation__c reservation = [
            SELECT Id, EndDate__c, StartDate__c, HouseName__c, HouseName__r.Name
            FROM Reservation__c
            WHERE StartDate__c = :startDate
            LIMIT 1
        ];
        delete reservation;
        Reservation__c newReservation = new Reservation__c(HouseName__c = reservation.HouseName__c, StartDate__c = reservation.StartDate__c, EndDate__c = reservation.EndDate__c);
        insert newReservation;
        System.assertNotEquals(Database.undelete(reservation, false).getErrors()[0].getMessage(), null);
        System.assertNotEquals(Database.undelete(reservation, false).isSuccess(), true);
        
        delete newReservation;
        System.assertEquals(Database.undelete(reservation,false).isSuccess(), true);
    }
}