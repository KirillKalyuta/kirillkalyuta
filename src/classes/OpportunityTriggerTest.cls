/**
 * Created by kalyutak on 8/24/2018.
 */
@isTest
private class OpportunityTriggerTest {

    @testSetup static void setup() {

        List<Opportunity> parOpportunities = new List<Opportunity>();
        for (Integer i = 0; i < 2; i++) {
            parOpportunities.add(new Opportunity(Name = 'Test' + i, StageName = 'Prospecting', CloseDate = Date.today(), RecordTypeId = OpportunityTriggerHandler.PARENT_RT));
        }
        insert parOpportunities;

        List<Opportunity> childOppList = new List<Opportunity>();
        for (Integer k = 0; k < parOpportunities.size(); k++) {
            Opportunity parOpp = parOpportunities[k];
            childOppList.add(new Opportunity(Name = 'Kirill1', StageName = 'Prospecting', CloseDate = Date.today(), RecordTypeId = OpportunityTriggerHandler.CHILD_RT, ParentOpp__c = parOpp.Id));
            childOppList.add(new Opportunity(Name = 'Kirill2', StageName = 'Value Proposition', CloseDate = Date.today(), RecordTypeId = OpportunityTriggerHandler.CHILD_RT, ParentOpp__c = parOpp.Id));
        }
        insert childOppList;

        Product2 prod = new Product2(
            Name = 'Product X',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;

        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = Test.getStandardPricebookId(),
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;

        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        for (Integer f = 0; f < childOppList.size(); f++) {
            Opportunity childOpp = childOppList[f];
            oliList.add(new OpportunityLineItem(OpportunityId = childOpp.Id, Quantity = 5, PricebookEntryId = pbEntry.Id, TotalPrice = 5 * pbEntry.UnitPrice));
        }
        insert oliList;
    }

    @isTest static void afterInsertTest() {
        Opportunity parOpp = [SELECT Id FROM Opportunity WHERE RecordTypeId = :OpportunityTriggerHandler.PARENT_RT LIMIT 1];
        Opportunity childOpportunity = new Opportunity(Name = 'Kirill5', StageName = 'Closed Won', CloseDate = Date.today(), RecordTypeId = OpportunityTriggerHandler.CHILD_RT, ParentOpp__c = parOpp.Id);
        insert childOpportunity;
        Opportunity childOpp = [SELECT ParentOpp__r.LeadingOpp__c FROM Opportunity WHERE Id = :childOpportunity.Id];
        System.assertEquals(childOpp.Id, childOpp.ParentOpp__r.LeadingOpp__c);
    }

    @isTest static void afterUpdateChildOppTest() {
        Opportunity childOpp = [SELECT Id, Probability FROM Opportunity WHERE RecordTypeId = :OpportunityTriggerHandler.CHILD_RT LIMIT 1];
        childOpp.Probability = 90;
        update childOpp;
        Opportunity updateChildOpp = [SELECT IsLeading__c, ParentOpp__r.LeadingOpp__c FROM Opportunity WHERE Id = :childOpp.Id];
        System.assertEquals(updateChildOpp.Id, updateChildOpp.ParentOpp__r.LeadingOpp__c);
    }

    @isTest static void afterUpdateParOppTest() {
        Opportunity childOpp = [
            SELECT Id, IsLeading__c, ParentOpp__c
            FROM Opportunity
            WHERE RecordTypeId = :OpportunityTriggerHandler.CHILD_RT
                AND IsLeading__c = FALSE
            LIMIT 1
        ];
        PricebookEntry pbEntry = [SELECT Id, UnitPrice FROM PricebookEntry];
        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>{
            new OpportunityLineItem(OpportunityId = childOpp.Id, Quantity = 10, PricebookEntryId = pbEntry.Id, TotalPrice = 10 * pbEntry.UnitPrice),
            new OpportunityLineItem(OpportunityId = childOpp.ParentOpp__c, Quantity = 5, PricebookEntryId = pbEntry.Id, TotalPrice = 5 * pbEntry.UnitPrice)
        };
        insert oliList;
        childOpp.Probability = 100;
        update childOpp;
        Opportunity parWithOli = [
            SELECT Id, LeadingOpp__c, (SELECT Id, Name, Quantity, ExternalKey__c FROM OpportunityLineItems)
            FROM Opportunity
            WHERE Id = :childOpp.ParentOpp__c
        ];
        System.assertEquals(3,parWithOli.OpportunityLineItems.size());
    }

    @isTest static void afterDeleteAndUndeleteChildOppTest() {
        Opportunity childLeadOpp = [
            SELECT Id, ParentOpp__c
            FROM Opportunity
            WHERE RecordTypeId = :OpportunityTriggerHandler.CHILD_RT
                AND IsLeading__c = TRUE
            LIMIT 1
        ];
        Set<Id> a = new Map<Id,OpportunityLineItem>([SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :childLeadOpp.Id]).keySet();
        System.assertNotEquals(0, a.size());
        Set<Id> b = new Map<Id,OpportunityLineItem>([SELECT Id FROM OpportunityLineItem WHERE ExternalKey__c IN :a]).keySet();
        System.assertNotEquals(0, b.size());
        delete childLeadOpp;
        System.assertEquals(0, [SELECT COUNT() FROM OpportunityLineItem WHERE Id IN :a]);
        System.assertEquals(0, [SELECT COUNT() FROM OpportunityLineItem WHERE Id IN :b]);
    }

    @isTest static void afterInsertOliTest() {
        Opportunity childOpp = [
            SELECT Id, IsLeading__c, ParentOpp__c
            FROM Opportunity
            WHERE RecordTypeId = :OpportunityTriggerHandler.CHILD_RT
                AND IsLeading__c = TRUE
            LIMIT 1
        ];
        PricebookEntry pbEntry = [SELECT Id, UnitPrice FROM PricebookEntry];
        OpportunityLineItem childOli = new OpportunityLineItem(OpportunityId = childOpp.Id, Quantity = 10, PricebookEntryId = pbEntry.Id, TotalPrice = 10 * pbEntry.UnitPrice);
        insert childOli;
        Opportunity opp = [
            SELECT Id, ParentOpp__c, (SELECT Id, OpportunityId FROM OpportunityLineItems)
            FROM Opportunity
            WHERE LeadingOpp__c = :childOpp.Id
        ];
        System.assertEquals(2,opp.OpportunityLineItems.size());
    }

    @isTest static void afterUpdateOliTest() {
        OpportunityLineItem childOli = [
            SELECT Id, ExternalKey__c, OpportunityId, Quantity
            FROM OpportunityLineItem
            WHERE Opportunity.RecordTypeId = :OpportunityTriggerHandler.CHILD_RT
                AND Opportunity.IsLeading__c = TRUE
            LIMIT 1
        ];
        childOli.Quantity = 10;
        update childOli;
        Opportunity parOpp = [
            SELECT Id, (SELECT Id, ExternalKey__c, OpportunityId, Quantity FROM OpportunityLineItems LIMIT 1)
            FROM Opportunity
            WHERE LeadingOpp__c = :childOli.OpportunityId
        ];
        System.assertEquals(10, parOpp.OpportunityLineItems[0].Quantity);
    }

    @isTest static void afterDeleteOliTest() {
        OpportunityLineItem childOli = [
            SELECT Id, ExternalKey__c, OpportunityId, Quantity
            FROM OpportunityLineItem
            WHERE Opportunity.IsLeading__c = TRUE
            LIMIT 1
        ];
        delete childOli;
        Opportunity parOpp = [
            SELECT Id, (SELECT Id, ExternalKey__c, OpportunityId, Quantity FROM OpportunityLineItems)
            FROM Opportunity
            WHERE LeadingOpp__c = :childOli.OpportunityId
        ];
        System.assertEquals(0,parOpp.OpportunityLineItems.size());
    }
}