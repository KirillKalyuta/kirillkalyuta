/**
 * Created by kalyutak on 8/31/2018.
 */

public class ReservationsOverlappingTriggerHandler extends TriggerHandler {


    private List<String> stringForQuerryList;
    private List<Reservation__c> newList;
    private Map<Id, Reservation__c> oldMap;

    public ReservationsOverlappingTriggerHandler() {
        this.newList = (List<Reservation__c>) Trigger.new;
        this.oldMap = (Map<Id, Reservation__c>) Trigger.oldMap;
        this.stringForQuerryList = new List<String>();
    }

    public override void beforeInsert() {
        checkedFreedomOfHouse();
    }

    public override void beforeUpdate() {
        checkedFreedomOfHouse();
    }

    public override void afterUndelete() {
        checkedFreedomOfHouse();
    }

    private void checkedFreedomOfHouse() {
        for (Reservation__c reservation : newList) {
            if (reservation.HouseName__c != null) {
                if (Trigger.isUpdate) {
                    Reservation__c oldReservation = oldMap.get(reservation.Id);
                    if (reservation.HouseName__c != oldReservation.HouseName__c || reservation.StartDate__c != oldReservation.StartDate__c || reservation.EndDate__c != oldReservation.EndDate__c) {
                        createStringForQuerry(reservation);
                    }
                } else {
                    createStringForQuerry(reservation);
                }
            }
        }
        if (!stringForQuerryList.isEmpty()) {
            String reservationString = 'SELECT StartDate__c, EndDate__c, HouseName__c, HouseName__r.Name ' +
                'FROM Reservation__c ' +
                'WHERE ' + String.join(stringForQuerryList, ' OR ');
            List<Reservation__c> existingReservationsOnHouses = Database.query(reservationString);
            Map<Id, List<Reservation__c>> exsitReservationsMap = new Map<Id, List<Reservation__c>>();
            if (!existingReservationsOnHouses.isEmpty()) {
                for (Reservation__c reservation : existingReservationsOnHouses) {
                    if (!exsitReservationsMap.containsKey(reservation.HouseName__c)) {
                        exsitReservationsMap.put(reservation.HouseName__c, new List<Reservation__c>());
                    }
                    exsitReservationsMap.get(reservation.HouseName__c).add(reservation);
                }
                for (Reservation__c newReservation : newList) {
                    for (Reservation__c existReservation : exsitReservationsMap.get(newReservation.HouseName__c)) {
                        if (newReservation.Id != existReservation.Id) {
                            if (!((newReservation.StartDate__c < existReservation.StartDate__c && newReservation.EndDate__c <= existReservation.StartDate__c) || (newReservation.StartDate__c >= existReservation.EndDate__c && newReservation.EndDate__c > existReservation.EndDate__c))) {
                                newReservation.addError(String.format(Label.TakenReservationError, new List<String>{
                                    existReservation.HouseName__r.Name,
                                    String.valueOf(existReservation.StartDate__c),
                                    String.valueOf(existReservation.EndDate__c)
                                }));
                            }
                        }
                    }
                    exsitReservationsMap.get(newReservation.HouseName__c).add(newReservation);
                }
            }
        }
    }

    private void createStringForQuerry(Reservation__c reservation) {
        String stringForFormat = '(HouseName__c = \'\'{0}\'\' AND (NOT((StartDate__c > {1} AND StartDate__c >= {2}) OR (EndDate__c <= {1} AND EndDate__c < {2}))){3})';
        String stringForQuerry = String.format(stringForFormat, new List<String>{
            reservation.HouseName__c,
            String.valueOf(reservation.StartDate__c),
            String.valueOf(reservation.EndDate__c),
            (Trigger.isBefore && Trigger.isUpdate) ? ' AND Id !=  \'' + reservation.Id + '\'' : ''
        });
        stringForQuerryList.add(stringForQuerry);
    }
}

