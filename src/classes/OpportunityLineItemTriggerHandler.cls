/**
 * Created by kalyutak on 8/24/2018.
 */
public class OpportunityLineItemTriggerHandler extends TriggerHandler {

    private Map<Id, OpportunityLineItem> newMap;
    private Map<Id, OpportunityLineItem> oldMap;
    private List<OpportunityLineItem> newList;
    private List<OpportunityLineItem> oldList;

    public OpportunityLineItemTriggerHandler() {
        this.newMap = (Map<Id, OpportunityLineItem>) Trigger.newMap;
        this.oldMap = (Map<Id, OpportunityLineItem>) Trigger.oldMap;
        this.newList = (List<OpportunityLineItem>) Trigger.new;
        this.oldList = (List<OpportunityLineItem>) Trigger.old;
    }

    public override void afterInsert() {
        upsertOliOnParent();
    }

    public override void afterUpdate() {
        upsertOliOnParent();
    }

    public override void afterDelete() {
        System.debug('***');
        deleteOliOnParent();
    }

    private void upsertOliOnParent() {
        Set<Id> oliIds = newMap.keySet();
        if (!oliIds.isEmpty()) {
            if (Trigger.isAfter) {
                String oliChildString = 'SELECT Id, Opportunity.ParentOpp__c, Quantity, TotalPrice, ExternalKey__c' + (Trigger.isInsert == TRUE ? ', PricebookEntryId ' : ' ') +
                    'FROM OpportunityLineItem ' +
                    'WHERE Id IN : oliIds ' +
                        'AND Opportunity.IsLeading__c = TRUE ' +
                        'AND Opportunity.ParentOpp__c != NULL';
                List<OpportunityLineItem> oliChildList = Database.query(oliChildString);
                if (!oliChildList.isEmpty()) {
                    List<OpportunityLineItem> oliParList = new List<OpportunityLineItem>();
                    for (OpportunityLineItem oliChild : oliChildList) {
                        OpportunityLineItem parOli = oliChild.clone(false, false, false, false);
                        parOli.OpportunityId = oliChild.Opportunity.ParentOpp__c;
                        parOli.ExternalKey__c = oliChild.Id;
                        oliParList.add(parOli);
                    }
                    upsert oliParList ExternalKey__c;
                }
            }
        }
    }

    private void deleteOliOnParent() {
        Set<Id> oliIds = oldMap.keySet();
        if (!oliIds.isEmpty()) {
            delete [SELECT Id, Name FROM OpportunityLineItem WHERE ExternalKey__c IN :oliIds];
        }
    }
}